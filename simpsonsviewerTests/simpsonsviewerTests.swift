//
//  simpsonsviewerTests.swift
//  simpsonsviewerTests
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import XCTest
import SwiftUI
@testable import simpsonsviewer

class simpsonsviewerTests: XCTestCase {
    @ObservedObject var characterListViewModel = CharacterListViewModel()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testAPIEndPoint() {
        // This is a functional test case to check if compilation codition is met and we are targetting the correct API
        let api = CharacterListViewModel.apiUrlString
        #if WIRE
            XCTAssertNotNil(api)
            XCTAssertEqual(api, "http://api.duckduckgo.com/?q=the+wire+characters&format=json")

        #else
            XCTAssertNotNil(api)
            XCTAssertEqual(api, "http://api.duckduckgo.com/?q=simpsons+characters&format=json")
        #endif
    }
    
    func testNumberOfCharactersToBeReturned() {
        // This is a functional test case to check if we get the correct number of objects from calling the API
        switch characterListViewModel.state {
        case .loading:
            print("It's still loading")
        case .fetched(let result):
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let object):
                #if WIRE
                XCTAssertNotNil(object.RelatedTopics.count)
                XCTAssertEqual(object.RelatedTopics.count, 63)

                #else
                XCTAssertNotNil(object.RelatedTopics.count)
                XCTAssertEqual(object.RelatedTopics.count, 43)
                #endif
            }
        }
    }
}
