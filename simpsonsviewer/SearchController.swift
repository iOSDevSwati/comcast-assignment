//
//  SearchController.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import Foundation
import SwiftUI

struct SearchController: UIViewRepresentable {
    
    @Binding var text : String
    class Coordinator: NSObject, UISearchBarDelegate {
        
        @Binding var text: String
        
        init(text: Binding<String>) {
            _text = text
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
        }
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            UIApplication.shared.endEditing(true)
        }
    }
    func makeCoordinator() -> SearchController.Coordinator {
        return Coordinator(text: $text)
    }
    func makeUIView(context: UIViewRepresentableContext<SearchController>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.placeholder = "Search Character"
        searchBar.delegate = context.coordinator
        return searchBar
    }
    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchController>) {
        uiView.text = text
    }
}

