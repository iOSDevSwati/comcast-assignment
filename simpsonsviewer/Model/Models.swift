//
//  Models.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import SwiftUI

struct object: Decodable {
    var RelatedTopics: [Character]
}

struct Character: Decodable & Hashable  {
    let Text: String
    let Icon: Icon
}

struct Icon: Decodable & Hashable  {
    let URL: String
}


//class CharacterViewModel: Identifiable {
//
//    let id = UUID()
//
//    let character: Character
//
//    init(character: Character) {
//        self.character = character
//    }
//
//    var name: String {
//        return self.character.Text.components(separatedBy: "-")[0]
//    }
//
//    var details: String {
//        var detailsString = self.character.Text.deletingPrefix(self.character.Text.components(separatedBy: "-")[0])
//        detailsString = detailsString.deletingPrefix(" -")
//        return self.character.Icon.URL
//    }
//
//    var image: String {
//        return self.character.Icon.URL
//    }
//}

