//
//  CharacterListViewModel.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

enum LoadableState<T> {
    case loading
    case fetched(Result<T, FetchError>)
}

enum FetchError: Error {
    case error(String)
    
    var localizedDescription: String {
        switch self {
        case .error(let message):
            return message
        }
    }
}

class CharacterListViewModel: ObservableObject {
    
    static let apiUrlString = Bundle.main.object(forInfoDictionaryKey: "EndPoint_URL") as! String
    var objectWillChange = PassthroughSubject<CharacterListViewModel, Never>()
    
    var state: LoadableState<object> = .loading {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    init() {
        guard let apiUrl = URL(string: CharacterListViewModel.apiUrlString) else {
            state = .fetched(.failure(.error("Malformed API URL.")))
            return
        }
        
        URLSession.shared.dataTask(with: apiUrl) { [weak self] (data, _, error) in
            if let error = error {
                self?.state = .fetched(.failure(.error(error.localizedDescription)))
                return
            }
            
            guard let data = data else {
                self?.state = .fetched(.failure(.error("Malformed response data")))
                return
            }
            let root = try! JSONDecoder().decode(object.self, from: data)
            DispatchQueue.main.async { [weak self] in
                self?.state = .fetched(.success(root))
            }
        }.resume()
    }
}

class ImageFetcher: ObservableObject {
    var objectWillChange = PassthroughSubject<Data, Never>()
    
    var data: Data = Data() {
        didSet {
            objectWillChange.send(data)
        }
    }
    
    init(url: String) {
        guard let imageUrl = URL(string: url) else {
            return
        }
        
        URLSession.shared.dataTask(with: imageUrl) { (data, _, _) in
            guard let data = data else { return }
            DispatchQueue.main.async { [weak self] in
                self?.data = data
            }
        }.resume()
    }
}

