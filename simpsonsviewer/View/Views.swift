//
//  Views.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import SwiftUI

struct ActivityIndicator: UIViewRepresentable {
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        uiView.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            uiView.stopAnimating()
            uiView.isHidden = true
        }
    }
}

struct LoadableImageView: View {
    @ObservedObject var imageFetcher: ImageFetcher

    init(with urlString: String) {
        imageFetcher = ImageFetcher(url: urlString)
    }
    
    var body: some View {
        if let image = UIImage(data: imageFetcher.data) {
            return AnyView(
                Image(uiImage: image).resizable()
            )
        } else {
            let noImageFound = UIImage(named: "noPhotoFound")!
            return AnyView(
                ZStack {
                    ActivityIndicator(style: .medium)
                    Image(uiImage: noImageFound).resizable()
                }
            )
        }
    }
}

