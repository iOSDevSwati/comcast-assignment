//
//  CharacterRow.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import SwiftUI

struct CharacterRow: View {
    var character: Character
    
    var body: some View {
        Text(character.Text.components(separatedBy: "-")[0])
    }
}
