//
//  CharacterDetailsView.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import SwiftUI

struct CharacterDetailsView : View {
    var character: Character
    
    var body: some View {
                   VStack {
                    LoadableImageView(with: character.Icon.URL).frame(width: 200, height: 200)
                    Divider()
                    Text(character.Text).padding(30)
                    .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(Color.blue, lineWidth: 4)
                    )
        }
        
    }
}

