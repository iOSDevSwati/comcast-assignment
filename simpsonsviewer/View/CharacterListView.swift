//
//  CharacterListView.swift
//  simpsonsviewer
//
//  Created by Swati Chawla on 11/24/19.
//  Copyright © 2019 ABC. All rights reserved.
//

import SwiftUI

struct CharacterListView : View {
    @ObservedObject var characterListViewModel = CharacterListViewModel()
    @State private var searchTerm: String = ""
    
    private var title: String {
        //use the flag we defined in custom swift flags under Build settings
        #if WIRE
            return "Wire Characters"
        #else
            return "Simpsons Characters"
        #endif
    }
    
    private var stateContent: AnyView {
        switch characterListViewModel.state {
        case .loading:
            return AnyView(
                ActivityIndicator(style: .medium)
            )
        case .fetched(let result):
            switch result {
            case .failure(let error):
                return AnyView(
                    Text(error.localizedDescription)
                )
            case .success(let object):
                return AnyView(
                    VStack{
                        SearchController(text: $searchTerm)
                        List {
                            ForEach(object.RelatedTopics.filter{
                                self.searchTerm.isEmpty ? true : $0.Text.localizedCaseInsensitiveContains(self.searchTerm)
                            }, id: \.self) { character in
                                NavigationLink(destination: CharacterDetailsView(character: character)) {
                                    CharacterRow(character: character)
                                }
                                
                            }
                        }.resignKeyboardOnDragGesture()
                    }
                )
            }
        }
    }
    
    var body: some View {
        NavigationView {
            stateContent
                .navigationBarTitle(title)
        }.navigationViewStyle(DoubleColumnNavigationViewStyle())
    }
}
